//2. Змініть текст усіх елементів h2 на "Awesome feature".

document.addEventListener("DOMContentLoaded", function() {
    var h2Elements = document.getElementsByClassName("h2");
    for (var i = 0; i < h2Elements.length; i++) {
        h2Elements[i].textContent = "Awesome feature";
    }
});