//1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.

var features1 = document.getElementsByClassName("feature");
console.log("Метод 1 (document.getElementsByClassName()):", features1);

for (var i = 0; i < features1.length; i++) {
    features1[i].style.textAlign = "center";
}

var feature2 = document.querySelector("#feature2");
console.log("Метод 2 (document.querySelector()):", feature2);

feature2.style.textAlign = "center";

